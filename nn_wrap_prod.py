import numpy as np
import tensorflow as tf
#import pandas as pd
from os import listdir
import math
import time
from ReadTFRecords import get_input
from tps_transformer import tps_stn
import scipy.misc as smi

batch_size = 2
epochs = 50
lr = 0.0001
def conv(batch_input, out_channels, stride, name):
	in_channels = batch_input.get_shape()[3]
	filter = tf.get_variable("filter"+name,[3, 3, in_channels, out_channels],dtype=tf.float32,initializer=tf.random_normal_initializer(0, 0.02))
	padded_input = tf.pad(batch_input, [[0, 0], [1, 1], [1, 1], [0, 0]], mode="CONSTANT")
	conv = tf.nn.conv2d(padded_input, filter, [1, stride, stride, 1], padding="VALID")
	return conv

def lrelu(x, a=0.2):
	with tf.name_scope("lrelu"):
		x = tf.identity(x)
		return (0.5 * (1 + a)) * x + (0.5 * (1 - a)) * tf.abs(x)

def batch_norm(inputs, is_training=True, decay=0.999):
	with tf.variable_scope("batchnorm"):
		scale = tf.Variable(tf.ones([inputs.get_shape()[-1]]))
		beta = tf.Variable(tf.zeros([inputs.get_shape()[-1]]))
		pop_mean = tf.Variable(tf.zeros([inputs.get_shape()[-1]]), trainable=False)
		pop_var = tf.Variable(tf.ones([inputs.get_shape()[-1]]), trainable=False)
		epsilon = 1e-5
		if is_training:
			batch_mean, batch_var = tf.nn.moments(inputs, axes=[0, 1, 2], keep_dims=False)
			train_mean = tf.assign(pop_mean,
                            pop_mean * decay + batch_mean * (1 - decay))
			train_var = tf.assign(pop_var,
                           pop_var * decay + batch_var * (1 - decay))
			with tf.control_dependencies([train_mean, train_var]):
				return tf.nn.batch_normalization(inputs,
                                        batch_mean, batch_var, beta, scale, epsilon)
		else:
			return tf.nn.batch_normalization(inputs,
                                      pop_mean, pop_var, beta, scale, epsilon)


def nn_model(product1,mask,wrap,tps_points):
	product = tf.concat([product1,mask], axis=-1) 
	conv1 = conv(product,64,2,"conv1")
	conv1 = batch_norm(conv1)
	conv1 = lrelu(conv1)
	conv2 = conv(conv1,128,2,"conv2")
	conv2 = batch_norm(conv2)
	conv2 = lrelu(conv2)
	conv3 = conv(conv2,256,2,"conv3")
	conv3 = batch_norm(conv3)
	conv3 = lrelu(conv3)
	conv4 = conv(conv3,512,2,"conv4")
	conv4 = batch_norm(conv4)
	conv4 = lrelu(conv4)
	conv5 = conv(conv4,1024,2,"conv5")
	conv5 = batch_norm(conv5)
	conv5 = lrelu(conv5)
	conv6 = conv(conv5,1024,2,"conv6")
	conv6 = batch_norm(conv6)
	conv6 = lrelu(conv6)
	conv7 = conv(conv6,2048,2,"conv7")
	conv7 = batch_norm(conv7)
	conv7 = lrelu(conv7)
        print conv7.shape
	shape1 = int(np.prod(conv7.get_shape()[1:]))
	flat = tf.reshape(conv7,[-1,shape1])
        print flat.shape
	filter1 = tf.get_variable("filter1",[shape1,2048],dtype = tf.float32,initializer=tf.random_normal_initializer(0, 0.02))
	fc1 = tf.matmul(flat,filter1)
	fc1 = lrelu(fc1)
	#shape1 = int(np.prod(fc1.get_shape()[1:]))
	filter2 = tf.get_variable("filter2",[2048,1024],dtype = tf.float32,initializer=tf.random_normal_initializer(0, 0.02))
	fc2 = tf.matmul(fc1,filter2)
	fc2 = lrelu(fc2)

	filter3 = tf.get_variable("filter3",[1024,800],dtype = tf.float32,initializer=tf.random_normal_initializer(0, 0.02))
	fc3 = tf.matmul(fc2,filter3)
	fc3 = tf.tanh(fc3)
	
	n = tf.reshape(fc3,[batch_size,800])
	out_points = tf.transpose(tf.reshape(n,[batch_size,2,400]),[0,2,1])
	tps_points = tf.transpose(tf.reshape(tps_points,[batch_size,2,400]),[0,2,1])
	print out_points.shape
	print tps_points.shape
	wrapped_tps = tps_stn(product1,20,20,out_points,[512,384,3])
	#original_warp = tps_stn(product1,20,20,tps_points,[512,384,3])

	out_points = tf.expand_dims(out_points, 1)
	tps_points = tf.expand_dims(tps_points, 1)
	#print out_points.shape

	loss_tps = tf.reduce_mean(tf.abs(out_points - tps_points))
	loss_image = tf.reduce_mean(tf.abs(wrapped_tps - wrap))

	loss = loss_image+loss_tps
	#original_warp = tps_stn(product1,20,20,tps_points,[512,384,3])
	gen_optim = tf.train.AdamOptimizer(0.00001, 0.999, 0.6)
	gen_train = gen_optim.minimize((loss), var_list=tf.trainable_variables())    
	#opt = tf.train.AdamOptimizer(learning_rate=lr,beta1 = 0.5,beta2 = 0.99).minimize(loss)
	global_step = tf.train.get_or_create_global_step()
	incr_global_step = tf.assign(global_step, global_step+1)    
	return {'loss':loss, 'train':tf.group(incr_global_step,gen_train), 
			'global_step':global_step, 'wrapped_tps':wrapped_tps, 'original_warp':wrap}

def main():
	(prod, mask,wrap, tps,src_id) = get_input()
	model = nn_model(prod, mask, wrap, tps)
	coord = tf.train.Coordinator()
	config = tf.ConfigProto()
	#config.gpu_options.visible_device_list= '3'
	sess = tf.Session(config=config)
	threads = tf.train.start_queue_runners(sess=sess, coord=coord)
	sess.run(tf.global_variables_initializer())
	m_folder = "jan_05"
	try: os.mkdir(m_folder)
	except: print("model directory already exists")
	try: os.mkdir(m_folder+'/train_outs')
	except: print("train_outs directory already exists")
	fetches = {'loss':model['loss'], 'train':model['train'],'global_step':model['global_step'], 'wrapped_tps':model['wrapped_tps'], 'original_warp':model['original_warp']}
	saver = tf.train.Saver(max_to_keep=2000)
	#saver.restore(sess, "./jan_05/model-1900")
	writer = tf.summary.FileWriter(m_folder+"/summary")
	writer.add_graph(sess.graph)
	maxsteps = 10000
	for i in range(maxsteps):
		result = sess.run(fetches)
        #[loss_o, train_o, wrapped_o,orig_warp_o] = sess.run([loss, train, wrapped,orig_warp])
		print ("global_step :", result['global_step'] , "loss is : ",result['loss'])
        #print model['wrapped_tps'][0].shape
		if result['global_step'] % 100 == 0 and result['global_step'] !=0:
			#saver.save(sess, m_folder+"/model" ,global_step=result["global_step"])
			smi.imsave('jan_05/train_outs/'+str(result['global_step'])+'_wrapped.png',result['wrapped_tps'][0])
			smi.imsave('jan_05/train_outs/'+str(result['global_step'])+'_wrapped_orig.png',result['original_warp'][0])
			#smi.imsave('./train_outs/'+str(result['global_step'])+'_wrapped_orig_tps.png',np.squeeze(model['another_warp_tps'][0]))


if __name__ == '__main__':
	main()
