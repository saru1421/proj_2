from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import json
import math
import os
import time
import numpy as np
import scipy.io as sio
import tensorflow as tf
from scipy.misc import imresize
import scipy.misc as smi
import numpy as np
import yaml
import random
from tensorflow.python import debug as tf_debug

from augment_tf import flip, random_brightness, gaussian_noise, central_scale_images_tf, NoAugment,random_brightness_3c,gaussian_noise_3c,flip1
config_path=yaml.load(open('configuration_path.yaml'))

input_file_pattern = config_path['TF_RECORD_OUT_PATH']+"train-?????-of-00008"
values_per_input_shard = 443
num_preprocess_threads = 1
#batch_size = 2
list_feature_values=[]
list_feature_names=[]

global select_func

def parse_tf_example(serialized, stage="",resize_height=4096,resize_width=3072):
    features_dict={'image_id':tf.FixedLenFeature([], tf.string),'height': tf.FixedLenFeature([], tf.int64),'width': tf.FixedLenFeature([], tf.int64)}
    for main_key in config_path['IMAGES_TF'].keys():
        features_dict[str(main_key)]=tf.FixedLenFeature([], tf.string)
    features = tf.parse_single_example(serialized,features=features_dict)
    img_id = features['image_id']
    height = tf.cast(features["height"], tf.int32)
    width = tf.cast(features["width"], tf.int32)

    images_dictionary={}
    images_types={}
    for name in config_path['IMAGES_TF'].keys():
    	images_types['encoded_'+str(name)]=config_path['IMAGES_TF'][name]['FORMAT']
    	if config_path['IMAGES_TF'][name]['FORMAT']=='JPEG':
    		temp_tf = tf.image.decode_jpeg(features[str(name)], channels=config_path['IMAGES_TF'][name]['CHANNELS'])
    		temp_tf = tf.image.convert_image_dtype(temp_tf, dtype=tf.float32)
    		#temp_tf = tf.reshape(temp_tf, tf.stack([4096,3072]))
		images_dictionary['encoded_'+str(name)] = tf.image.resize_images(temp_tf,size=[resize_height, resize_width],method=tf.image.ResizeMethod.BILINEAR)
                
    	elif config_path['IMAGES_TF'][name]['FORMAT']=='PNG':
    		temp_tf=tf.image.decode_png(features[str(name)], channels=config_path['IMAGES_TF'][name]['CHANNELS'])
    		temp_tf = tf.image.convert_image_dtype(temp_tf, dtype=tf.float32)
		#print(temp_tf.get_shape())
    		images_dictionary['encoded_'+str(name)] = tf.image.resize_images(temp_tf,size=[resize_height, resize_width],method=tf.image.ResizeMethod.BILINEAR)
    	else:
    		images_dictionary['encoded_'+str(name)]=features[str(name)]

    list_feature_values.append(img_id)
    list_feature_names.append('img_id')
    
    rand_var_bright = tf.random_uniform([1],minval=-0.3,maxval=0.3,dtype = tf.float32)
    #rand_var_scale = random.randint(10,25)
    rand_var_scale = tf.random_uniform([1],minval=0.91,maxval=0.98,dtype = tf.float32)
    rand_var_noise = tf.random_uniform([1],minval=0.1,maxval=0.2,dtype = tf.float32)

    function_option = tf.convert_to_tensor([1,2,3,4,5])#1-brightness,2-central_scale,3-flip,4-gaussian_noise,5-NoAugment
    prob = tf.multinomial(tf.log([[0.5]*5]), 1)    
    select_func = function_option[tf.cast(prob[0][0], tf.int32)]
    #select_func = np.random.choice([1,2,3,4,5],p=[0.2,0.2,0.2,0.2,0.2])
    """
    for name in config_path['IMAGES_TF'].keys():
      if config_path['IMAGES_TF'][name]['CHANNELS'] == 4:
		    name = 'encoded_'+str(name)
		    images_dictionary[name] = tf.case([(tf.equal(select_func,1),lambda: random_brightness(images_dictionary[name],rand_var_bright)),
                         (tf.equal(select_func, 2), lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale)),
                         (tf.equal(select_func, 3), lambda: flip(images_dictionary[name])),
                         (tf.equal(select_func, 4), lambda: gaussian_noise(images_dictionary[name],rand_var_noise)),
                         (tf.equal(select_func, 5), lambda: NoAugment(images_dictionary[name]))],lambda:NoAugment(images_dictionary[name]))
      
      elif config_path['IMAGES_TF'][name]['CHANNELS'] == 3:
		    name = 'encoded_'+str(name)
		    images_dictionary[name] = tf.case([(tf.equal(select_func, 1), lambda: random_brightness_3c(images_dictionary[name],rand_var_bright)),
                         (tf.equal(select_func, 2), lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale)),
                         (tf.equal(select_func, 3), lambda: flip(images_dictionary[name])),
                        (tf.equal(select_func, 4), lambda: gaussian_noise_3c(images_dictionary[name],rand_var_noise)),
                        (tf.equal(select_func, 5), lambda: NoAugment(images_dictionary[name]))],lambda:NoAugment(images_dictionary[name]))
      
      elif config_path['IMAGES_TF'][name]['CHANNELS'] == 1:
                    name = 'encoded_'+str(name)
                    print(name)
                    pred1 = tf.equal(select_func,3)
                    pred2 = tf.equal(select_func,2)
                    images_dictionary[name] = tf.case([(pred1,lambda: flip(images_dictionary[name])),(pred2,lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale))],lambda:NoAugment(images_dictionary[name]))
                    #images_dictionary[name] = tf.case([(tf.equal(select_func,3),lambda: flip(images_dictionary[name])),(tf.equal(select_func,2),lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale))],lambda:NoAugment(images_dictionary[name]))
      """

    #def apply_aug(option,function,channels):
    #  if channels
    for name_config in config_path['IMAGES_TF'].keys():
      name = 'encoded_'+str(name_config) 
      #rand_var_scale = random.randint(10,25)
      print(name)             
      pred1 = tf.equal(select_func,3)
      pred2 = tf.equal(select_func,2)
      images_dictionary[name] = tf.case([(pred1,lambda: flip(images_dictionary[name]))],
				lambda:images_dictionary[name])
      images_dictionary[name] = tf.case([(pred2,lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale))],
                                lambda:images_dictionary[name])
      """
      images_dictionary[name] = tf.case([
                         (tf.equal(select_func, 3), lambda: flip(images_dictionary[name]))                         
                         ],lambda:NoAugment(images_dictionary[name]))
      
      images_dictionary[name] = tf.case([
                        (tf.equal(select_func, 2), lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale)) 
                         ],lambda:NoAugment(images_dictionary[name]))

      """ 
      if config_path['IMAGES_TF'][name_config]['CHANNELS'] == 4:
                    #name = 'encoded_'+str(name)
                    images_dictionary[name] = tf.case([(tf.equal(select_func,1),lambda: random_brightness(images_dictionary[name],rand_var_bright)),
                        (tf.equal(select_func, 4), lambda: gaussian_noise(images_dictionary[name],rand_var_noise))
                        ],lambda:NoAugment(images_dictionary[name]))
       
      if config_path['IMAGES_TF'][name_config]['CHANNELS'] == 3:
                    #name = 'encoded_'+str(name)
                    print('name in if condition,',name)
                    images_dictionary[name] = tf.case([(tf.equal(select_func, 1), lambda: random_brightness_3c(images_dictionary[name],rand_var_bright)),
                        (tf.equal(select_func, 4), lambda: gaussian_noise_3c(images_dictionary[name],rand_var_noise))
                        ],lambda:NoAugment(images_dictionary[name]))
      
      """
      images_dictionary[name] = tf.case([
                         (tf.equal(select_func, 3), lambda: flip(images_dictionary[name])),
                         (tf.equal(select_func, 2), lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale))
                         ],lambda:images_dictionary[name])
      """
      """
      elif config_path['IMAGES_TF'][name]['CHANNELS'] == 1:
                    name = 'encoded_'+str(name)
                    print(name)
                    pred1 = tf.equal(select_func,3)
                    pred2 = tf.equal(select_func,2)
                    images_dictionary[name] = tf.case([(pred1,lambda: flip(images_dictionary[name])),
			(pred2,lambda: central_scale_images_tf(images_dictionary[name],rand_var_scale))],
			lambda:NoAugment(images_dictionary[name]))
      """
    
    for name in images_dictionary.keys():
    	images_dictionary[str(name)] = (images_dictionary[str(name)] - 0.5) * 2.0
		    
    
    for name in config_path['IMAGES_TF'].keys():
      list_feature_values.append(images_dictionary['encoded_'+str(name)])
      list_feature_names.append(name)
	
	
    print('<-----------feature_label_names----------->')
    print(list_feature_names,"features")
    return [list_feature_names,list_feature_values]
    #return inout
    
def prefetch_input_data(reader,
                        file_pattern,
                        is_training,
                        batch_size,
                        values_per_shard,
                        input_queue_capacity_factor=16,
                        num_reader_threads=1,
                        shard_queue_name="filename_queue",
                        value_queue_name="input_queue"):
        data_files = []
        for pattern in file_pattern.split(","):
                data_files.extend(tf.gfile.Glob(pattern))
        if not data_files:
                tf.logging.fatal("Found no input files matching %s", file_pattern)
        else:
                tf.logging.info("Prefetching values from %d files matching %s",
                                                                                len(data_files), file_pattern)

        if is_training:
                filename_queue = tf.train.string_input_producer(
                                data_files, shuffle=True, capacity=16, name=shard_queue_name)
                min_queue_examples = values_per_shard * input_queue_capacity_factor
                capacity = min_queue_examples + 100 * batch_size
                values_queue = tf.RandomShuffleQueue(
                                capacity=capacity,
                                min_after_dequeue=min_queue_examples,
                                dtypes=[tf.string],
                                name="random_" + value_queue_name)
        else:
                filename_queue = tf.train.string_input_producer(
                                data_files, shuffle=False, capacity=1, name=shard_queue_name)
                capacity = values_per_shard + 3 * batch_size
                values_queue = tf.FIFOQueue(
                                capacity=capacity, dtypes=[tf.string], name="fifo_" + value_queue_name)

        enqueue_ops = []
        for _ in range(num_reader_threads):
                _, value = reader.read(filename_queue)
                enqueue_ops.append(values_queue.enqueue([value]))
        tf.train.queue_runner.add_queue_runner(tf.train.queue_runner.QueueRunner(
                        values_queue, enqueue_ops))
        tf.summary.scalar(
                        "queue/%s/fraction_of_%d_full" % (values_queue.name, capacity),
                        tf.cast(values_queue.size(), tf.float32) * (1. / capacity))

        return values_queue


def get_input(batch_size,process_image_h=4096,process_image_w=3072):
  # Load input data
  input_queue = prefetch_input_data(
      tf.TFRecordReader(),
      input_file_pattern,
      is_training=True,
      batch_size=batch_size,
      values_per_shard=values_per_input_shard,
      input_queue_capacity_factor=2,
      num_reader_threads=num_preprocess_threads)

  # Image processing and random distortion. Split across multiple threads
  images_and_maps = []

  for thread_id in range(num_preprocess_threads):
    serialized_example = input_queue.dequeue()
    [names,values] = parse_tf_example(serialized_example,resize_height=process_image_h,resize_width =process_image_w)
    #print('images and names',names,values)
    images_and_maps.append(values)

  # Batch inputs.
  queue_capacity = (7 * num_preprocess_threads *
                    batch_size)
  #return flipped_image
  return tf.train.batch_join(images_and_maps,
                             batch_size=batch_size,
                             capacity=queue_capacity,
                             name="batch")


if config_path['CHECK_SAMPLE_DATA']=='True':
	total_values = get_input(config_path['READ_BATCH_SIZE'],config_path['READ_HEIGHT'],config_path['READ_WIDTH'])
	#print(total_values,'total_values---------------------')
	coord = tf.train.Coordinator()
	config = tf.ConfigProto()
	config.gpu_options.visible_device_list= '1'
	sess = tf.Session(config=config)
	sess.run(tf.local_variables_initializer())
	sess.run(tf.global_variables_initializer())
	threads = tf.train.start_queue_runners(sess = sess, coord=coord)
	
	for i in range(50):
		#import ipdb
		#ipdb.set_trace()
		total_values_np = sess.run(total_values)	
		try:
			os.makedirs(config_path['CHECK_SAMPLE_STORAGE_PATH'])
		except:
			pass
		for idx,list_feature_name in enumerate(list_feature_names):
			if(list_feature_name == 'img_id'):
				name = total_values_np[idx]
				continue
			smi.imsave(config_path['CHECK_SAMPLE_STORAGE_PATH']+name[0]+'_'+str(list_feature_name)+"_"+str(i)+'.png',np.squeeze(total_values_np[idx]))
		print('xxx-----saved-----x')
